const express = require('express')
const app = express()

app.use('/', require('./routes/home'))// home is file name.

app.listen(3000, 'localhost', ()=>{
    console.log('Listening on port:3000');
})

