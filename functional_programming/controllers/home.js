exports.home = (req, res, next) => {
    res.json(a)
}

const ages = [9, 33, 11, 23, 12, 25, 34,59, 80]
const ageMap = ages.map(age => Math.sqrt(age)).map(age => age * 2)
//console.log(ageMap)

const pets = [
    {name: "manee", species: "cat", age: 4},
    {name: "sodsai", species: "dog", age: 2},
    {name: "namfon", species: "cat", age: 6},
    {name: "sookjai", species: "dog", age: 2}
];


//Map return brand new array
const nameOnly =  pet =>  pet.name;
const a = pets.map(nameOnly)
//console.log(a);

//Filter matches condition and returns brand-new array.
const findDogs = pet => pet.species == 'dog';
const b = pets.filter(findDogs);
//console.log(b);

const findBabies = pet => pet.age < 3;
const c = pets.filter(findBabies);
//console.log(c)

//Find baby dogs
const babyDogs = pets.filter(findDogs).filter(findBabies);
//console.log(babyDogs)

//Find baby dog's names
const babyDogNames = babyDogs.map(nameOnly);
//console.log(babyDogNames)

//Array.sort()
const sortedAge = pets.sort((a, b) => a.age > b.age ? 1 : -1)
//console.log(sortedAge)

const sortedAge2 = ages.sort((a,b)=> b - a)
console.log(sortedAge2)
