Express-generator application  is for auto generate express project.

- download and install nodeJS comes with npm
- node -v  //check node version
- npm -v  //check npm version
- go to expressjs.com for instructions 
-  mkdir foldername  //create new foldername
-  cd foldername
- sudo npm install express-generator -g
- express --view=pug myapp  // myapp is for project name
- cd myapp
- npm install  //install dependencies
- DEBUG=myapp:* npm start  //Start server and run the application
- check out http://localhost:3000
- press ctrl+c  to stop server

