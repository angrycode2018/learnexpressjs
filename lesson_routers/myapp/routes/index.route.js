var express = require('express');
var router = express.Router();

let control = require('../controllers/home.controller');

/* GET home page. */
router.get('/', control.home);
router.post('/', control.submit_lead);
router.all('/secret', control.secret);
router.get('/about', control.about);
router.get('/something.text', (req, res, next) => {
    res.send('Hello, man')
});

// ต้องลงท้ายด้วย cd ส่วน b จะมีหรือไม่มีก้อได้ แต่ต้องมี a
//router.get('/ab?cd', control.abcd1);

//ต้องลงท้ายด้วย cd ส่วน b มีตั้งแต่ 1 ตัวขึ้นไป และต้องมี a
//router.get('/ab+cd', control.abcd2); 

//ขึ้นต้นด้วย ab และลงท้ายด้วย cd  ตรงกลางจะเป็นอะไร และมีจำนวนกี่ตัวก้อได้
//router.get('/ab*cd', control.abcd3);

//ขึ้นต้นด้วย ab และลงท้ายด้วย e  ส่วน cd จะมีหรือไม่มีก้อได้
//router.get('/ab(cd)?e', control.abcd4);

//ขอให้มีตัวอักษร a อยู่ใน path 
// /a/ ไม่ต้องมีเครื่องหมาย quote ครอบ
//router.get(/a/, control.a);

//ต้องลงท้ายด้วย fly ข้างหน้าเป็นอะไร มีกี่ตัวก็ได้
//router.get(/.*fly$/, control.butterfly);


//Multiple Parameters
router.get('/users/:userId/books/:bookId', (req, res, next)=>{
    res.send(req.params);//{userId: 33, bookId:3939}
});

//แยก parameters 2ตัว ด้วยเครื่องหมาย "-" 
router.get('/flights/:from-:to', (req, res, next)=>{
    res.send(req.params);//{"from": "BKK", "to": "TKO"}
});

//แยก parameters 2ตัว ด้วยเครื่องหมาย "." 
router.get('/flights/:from.:to', (req, res, next)=>{
    res.send(req.params);//{"from": "BKK", "to": "TKO"}
});

// userId ต้ดงเป็นตัวเลขเท่านั้น
router.get('/user/:userId(\\d+)', (req, res, next)=>{
    res.send(req.params);
});

// Next()
router.get('/example/b', (req, res, next) => {
    console.log('The next function will handle response.');
    next();
}, (req, res)=>{
    res.send("I am the second function who send the response to you.")
});

//Array callbacks
let cb0 = (req, res, next)=>{
    console.log('Callback 0');
    next();
};
let cb1 = (req, res, next)=>{
    console.log('Callback 1');
    next();
};
let cb2 = (req, res, next)=>{
    res.send('I am the last callback.');
};

router.get('/example/cb', [cb0, cb1, cb2]);

//Array callbacks + callback with next() + last callback 
//res.msg = "some text";  msg is new prop of res which can be passed to next function.
router.get('/example/cb2', [cb0, cb1], (req, res, next)=>{
     res.msg = "This is message ";
    console.log('The response will be sent by the last function')
    next();
},(req, res)=>{ res.send( res.msg +' from the last function.')});


//Router.route()
router.route('/book')
.get((req, res) =>{
    res.send('Get request.')
})
.post((req, res) =>{
    res.send('Post request')
})
.put((req, res) =>{
    res.send('Put request')
});

//Pug Language
router.get('/basicpug', (req, res, next) => {
    res.render('basicpug');
});

module.exports = router; 
