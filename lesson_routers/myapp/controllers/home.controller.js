
// This controller is exported to /myapp/routes/index.js
exports.home = function(req, res, next) {
    //render file home.pug
    res.render('home', { title: 'Thailand' }); 
  };

exports.submit_lead = function(req, res, next) {
  console.log("Lead Email:", req.body.lead_email);
  res.redirect('/');
};

exports.secret = function(req, res, next) {
  //render file secret.pug
  res.render('secret')
}

exports.about = (req, res, next) => {
  res.send('About Page')
}

exports.abcd1 = (req, res, next) => {
  res.send('ab?cd');
}

exports.abcd2 = (req, res, next) => {
  res.send('ab+cd');
}
exports.abcd3 = (req, res, next) => {
  res.send('ab*cd');
}
exports.abcd4 = (req, res, next) => {
  res.send('ab(cd)?e');
}
exports.a = (req, res, next) => {
  res.send('/a/');
}

exports.butterfly = (req, res, next) => {
  res.send('Butterfly');
}