exports.rest = (req , res, next) => {
    res.json(otherColors)
}

// Rest Items
const rainbow = ['red', 'orange', 'yellow', 'green', 'blue', 'indigo', 'violet'];

// Assign the first and third items to red and yellow
// Assign the remaining items to otherColors variable using the spread operator(...)
const [red,, yellow, ...otherColors] = rainbow;

console.log(otherColors); // ['green', 'blue', 'indigo', 'violet']


//Rest Items
const organizations = ['Pyke', 'Black Sun', 'Kanjiklub', 'Crimson Dawn'];
const [firstGang, secondGang, ...theRest] = organizations;

console.log(firstGang); // 'Pyke'
console.log(secondGang); // 'Black Sun'
console.log(theRest); // ['Kanjiklub', 'Crimson Dawn']