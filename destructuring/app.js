const express = require('express')
const app = express()

app.use('/', require('./routes/home'))

app.listen(3000, 'localhost', ()=>{
    console.log('Listening port:3000')
})

