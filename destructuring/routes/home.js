const express = require('express');
const router = express.Router();


router.get('/', require('../controllers/home').home);
router.get('/rest', require('../controllers/rest').rest);
router.get('/default', require('../controllers/default').default);
router.get('/skip', require('../controllers/skip').skip);
router.get('/swap', require('../controllers/swap').swap);

module.exports = router;