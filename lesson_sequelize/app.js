const express = require('express')
const app = express()

const Sequelize = require('sequelize');

const sequelize = new Sequelize('test', 'root', '1234', {
  host: 'localhost',
  dialect: 'mysql'
});

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

    
app.get('/', (req, res, next)=> {
    res.send('Sequelize...')
})

app.listen(3000, 'localhost', ()=>{
    console.log('Express starts on port:3000')
})