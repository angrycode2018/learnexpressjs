const models = require('../models');

//Route:get'/', Controller:get_landing, Model:null, View:landing_view.pug
exports.get_landing = function(req, res, next) {
    //send object with property title to index.pug and then send index.pug back to client.
    res.render('landing_view', { title: 'Hello, welcome', user: req.user });
  }

//Insert new lead email to DB  
//Route:post'/', Controller:submit_lead, Model:Lead.create, View:landing_view.pug
exports.submit_lead = (req, res , next) => {
  console.log('Lead email is '+req.body.lead_email);
  return models.Lead.create({
    email: req.body.lead_email
  }).then(lead => {
    res.redirect('/leads');
  });
}

//Show all lead emails
//Route:'/leads', Controller:show_leads, Model:Lead.findAll, View: leads.pug
exports.show_leads = (req, res, next) => {
 return models.Lead.findAll().then(leads => {
   let page = 'lead/leads';
   let data = {'title': 'Show Leads', 'leads': leads};
    res.render(page, data);
  });
}

//Show one lead email
//Method:Get, Route:/lead/:lead_id, Controller:show_lead, Model:Lead.findOne, View:lead.pug
exports.show_lead = (req, res, next) => {
  console.error('Show one lead.....');
 return models.Lead.findOne({
    'where': {'id': req.params.lead_id}
  }).then(lead => {
    console.error('Dot Then.....');
    res.render('lead/lead', {'title': 'single email', 'lead': lead});
  });
}

//Show form for editing email
//Method:Get, Route: '/lead/:lead_id/edit', Controller: 'show_edit_lead', View: 'edit_lead.pug'
exports.show_edit_lead = (req, res, next) => {
  console.log('Show edit lead');
  return models.Lead.findOne({
    'where': {
      'id': req.params.lead_id
    }
  }).then(lead => {
    res.render('lead/edit_lead', {'lead': lead})
  });
}

//Update new email into DB
//Method:POST, Route: '/lead/:lead_id/edit', Controller: 'edit_lead'
exports.edit_lead = (req, res, next) => {
  console.log('New email: ' + req.body.lead_email);
  const newEmail = {email: req.body.lead_email};
  const options = {where: {id: req.params.lead_id}};
  const onComplete = result => {
    console.log('UPDATE Lead Successfully');
    res.redirect('/lead/'+req.params.lead_id);
  }

  return models.Lead.update( newEmail, options).then(onComplete);
}

//Delete lead email from DB
//Method:POST, Route:'/leads/:lead_id/delete', Controller:'delete_lead', View:'landing_view.pug'
exports.delete_lead = (req, res, next) => {
  const options = {where: {id: req.params.lead_id }};
  const onComplete = result => res.redirect('/leads');

  return models.Lead.destroy(options).then(onComplete);
}

//Using Jquery Ajax to delete lead email from DB
//Method:POST, Route:'/leads/:lead_id/delete_json', Controller:'delete_lead_json', View:''
exports.delete_lead_json = (req, res, next) => {
  const options = {where: {id: req.params.lead_id }};
  const onComplete = result => res.send({msg: "Success"});

  return models.Lead.destroy(options).then(onComplete);
}