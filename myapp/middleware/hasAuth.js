 let createError = require('http-errors');

 exports.isLoggedIn = function(req, res, next) {
     if(req.user)
        next();//Call the next route handler.
     else 
     //Skip all the routes handlers comming next.
        next(createError(404, "Page does not exist."));
 }
 
//Only admin user can access to leads content.
 exports.hasAuth = function(req, res, next) {
     if(req.user && req.user.is_admin == true)
        next();//Call the next route handler.
     else 
     //Skip all the routes handlers comming next.
        next(createError(404, "Page does not exist."));
 }
 