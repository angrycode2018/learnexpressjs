หลังจากทำการสร้างโปรเจกต์ด้วย express-generator เสร็จแล้ว
- https://www.youtube.com/watch?v=G8uL0lFFoN0
- npm install // Add dependencies
- DEBUG=myapp:* npm start // To Start project 
- check out http://localhost:3000
- Create Controllers folder to keep all callback handler for routers
- Add .gitignore file
- Check out https://getbuzz.io/c/learning-expressjs
- npm install nodemon --save-dev // save nodemon as developer dependency only. devDependencies is added to package.json file
  "devDependencies": {
    "nodemon": "^1.19.1"
  }

-  Update "scripts" property in package.json as below.
  "scripts": {
    "start": "if [[ $NODE_ENV == 'production' ]]; then node ./bin/www; else nodemon ./bin/www; fi"
  }

- Add a form with email input
  - views : input name="lead_email" 
  - controllers : req.body.lead_email

- install Homebrew  is a package manager for MacOS
    go to https://brew.sh  follow instructions

- Install PostgreSQL and create database
    command "brew install postgresql"
    wait until "Udating Homebrew" is completed.
    command: "brew services start postgresql"
    command: "psql postgres" //To connect database
    Now you should see "postgres=#" command prompt for postgresql.
    Learning psql basic commands, visit http://www.postgresqltutorial.com/psql-commands/
    command: CREATE ROLE "admin" WITH LOGIN PASSWORD '1234';
    command: CREATE DATABASE "express-demo";
    keybord: contrl+d  //To exit

Sequelize is an CRUD database accessible library (ORM) Object Relational Mapper
- Install Sequelize http://docs.sequelizejs.com/manual/getting-started.html
  command: npm install sequelize --save
  command: npm install pg --save  //Low-level driver library for accessing postgresql DB
  command: npm install sequelize-cli -g // Command Line for sequelize

- Setup configuration file path for sequelize cli to access database
    1.create file ".sequelizerc" at root directory (myapp)
    2.Modify file ".sequelizerc" as the link below
    https://github.com/buzz-software/expressjs-mvp-landing-page/blob/master/.sequelizerc
  
    3.Command: sequelize init // Sequelize will auto generates folder config , migrations, models, and seeders

- Modify file 'config.js' as below
module.exports = {
  "development": {
    "username": "admin",
    "password": '1234',
    "database": "express-demo",
    "host": "127.0.0.1",
    "dialect": "postgres",
    "port": 5432,
    "operatorsAliases": false
  },

- Migrations & Models (CRUD)
  // Migrations is for manipulating table such as "creating a table", "adding a column" a representation of models. 
  << [Optional] You can generate the migration and model from the command line if you installed sequelize-cli:
Example:
`sequelize model:generate --name Leads --attributes email:string`
*Note:* Sequelize automatically generates the id, updatedAt and createdAt fields. [Optional] />>

  1.Create "migrations" folder under root directory 
  1.2 Create file "201901041917-CreateLeadTable.js" under migrations folder
  1.3 To create table named "Lead" Run Command: "sequelize db:migrate"
    incase of failure with message : "Error: Please install pg package manually" 
    ,run command "npm install pg --save" to reinstall pg package.
    and then run command "npm audit fix" (optional).
    finally run command "sequelize db:migrate" again.

  // Models is used by Sequelize for CRUD operations.
  2.Create "Lead.js" file (represent model Lead) under "models" folder as link below
  https://github.com/buzz-software/expressjs-mvp-landing-page/blob/master/models/Lead.js
  
  3.Modify "www" file under bin folder as link below
  https://github.com/buzz-software/expressjs-mvp-landing-page/blob/master/bin/www

  4.Import models into file "landing.js" under folder "controllers" and then call method "models.Lead.create()"

  5. Now user-email sending from email input is able to be inserted into table 'Lead' in database.

  6. Show all emails from Lead table (models.Lead.findAll())
  https://gitlab.com/angrycode2018/learnexpressjs/commit/96f28c5e5cbd91610b84d2edb7d468c6f3be0658

  7. Show single email from Lead table (models.Lead.findOne())
  https://gitlab.com/angrycode2018/learnexpressjs/commit/82721ca5638f573ea3a48730fe8db4b828432c6b

  8. Edit lead email using "models.Lead.update()"

  9. Delete lead email from DB using 'models.Lead.destroy()'

  10. Use JQuery.ajax to delete lead email from DB 

- Adding Bootstrap into project
  //Part I: include "head.pug" and "footer.pug" into "landing_view.pug"
    1. Create new "common" folder under "views" folder next create new files including "head.pug, navbar.pug, footer.pug"
    2. Visit link https://getbootstrap.com/docs/4.3/getting-started/download/ and looking for BootstrapCDN section, 
    next copy all Bootstrap links provided in this section and paste them into "head.pug" and "footer.pug"
    3. Reorder "script" tag in the file "footer.pug" as follow "jquery.js, popper.js, bootstrap.js"
    4. Modify "landing_view.pug" by including "head.pug" and "footer.pug"
    5. Modify the file "lead.pug" and delete the file "layout.pug"

  //Part II: include navbar
    1. Modify "navbar.pug" and include one into "landing_view.pug"

  //Part III: Adding "div.container" and "table" tags into "landing_view.pug"
    1. Replace jquery-3.3.1.slim.min.js to be jquery-3.3.1.min.js as slim version do not support Ajax
    2. Adding div.container and table into file "landing_view.pug"

  //Part IV: Clean code and complete Bootstrap

- LogIn & SignUp
  1. Create Router,Controller,and View for login and signup 
  2. Create User model for users and migration for db table
  3. Run "sequelize db:migrate" to create table "Users"
  4. Install NodeJS library called "Passport" to authenticates users for more infos visit www.passportjs.org
    4.1 Run "npm install passport passport-local bcrypt validator express-session connect-flash --save"
  5. Create "passport_setup.js" for user authentication.
  6. Define controller methods into user.js file for handling '/login', '/signup' routes 
  7. In 'migration' directory, creates file names 'addIsAdminToUsers'
  8. Create controller method for routes '/logout'
  9. When signup or login successfully, pass 'user' object to View in order that user-email can be shown on navbar 
  10. Debug 'req.flash is not a function'

- Form validation
  1. Run 'npm install validator lodash --save' 
  Todo: Add instructions to compleate form validation section.

- Access control & Middleware
  Objectives : Only loggedIn-adminUser can access to leads content.
  1. Only loggedIn user can access leads email page. https://gitlab.com/angrycode2018/learnexpressjs/commit/2892bd6a8d5fe8d2bd0efd7f50f7c4158cbe1949
  2. Skip this step if Migration "addIsAdminToUsers.js" is already existed. If not, create new one and then run "sequelize db:migrate".
  3. Skip this step if "controllers/user.js" and signup method has been already updated.
  4. Skip this step if "models/User.js" is_Admin field is already existed.
  5. Test generating the migration and model "Ohms" using command line `sequelize model:generate --name Ohms --attributes email:string` followed by "sequelize db:migrate"

//To Do : Fix bug fail to INSERT INTO is_admin field as a result nobody has role of admin;
//The End of Tutorial...
