var express = require('express');
var router = express.Router();
const landing = require('../controllers/landing')//../foldername/filename
const user = require('../controllers/user');
let { isLoggedIn, hasAuth } = require('../middleware/hasAuth.js');

router.get('/login', user.show_login);
router.get('/signup', user.show_signup);
router.post('/login', user.login);
router.post('/signup', user.signup);
router.post('/logout', user.logout);
router.get('/logout', user.logout);

// const noop = function(req, res, next) {
//     console.log('Midleware is working...')
//     next();
// }

/* GET home page. */
router.get('/', landing.get_landing);
router.post('/', landing.submit_lead);//Insert new lead email to DB
router.get('/leads', hasAuth, landing.show_leads);//Show all lead emails
router.get('/lead/:lead_id', hasAuth, landing.show_lead);//Show one lead email
router.get('/lead/:lead_id/edit', hasAuth, landing.show_edit_lead);//Show edit form
router.post('/lead/:lead_id/edit', hasAuth, landing.edit_lead);//Update new email to DB
router.post('/lead/:lead_id/delete', hasAuth, landing.delete_lead);//Delete lead email from DB
router.post('/lead/:lead_id/delete-json', hasAuth, landing.delete_lead_json);//Use JQuery.ajax to delete lead email from DB

module.exports = router;
